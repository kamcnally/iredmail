# kamcnally/iRedMail #

* [Overview](#overview)
* [Creating a New Instance](#creating-a-new-instance)
   * [Customise iRedMail Configuration](#customise-iredmail-configuration)
   * [Creating the Container](#creating-the-container)
      * [Ports](#ports)
      * [Optional Variables](#optional-variables)
* [Getting and Sending Mail](#getting-and-sending-mail)
* [Keeping Mail Out Of SPAM Filters (DNS Records)](#keeping-mail-out-of-spam-filters-dns-records)
* [Update SSL Certificates](#update-ssl-certificates)
* [Planned Features](#planned-features)


## Overview ##
This is [iRedMail](http://iredmail.org/) in a Docker container. The aim is to bring an easy to deploy mail server, such as iRedMail, to a Docker container.


[iRedMail](http://iredmail.org/) is designed to be installed on a fresh installation, therefore this container will follow the same rule. Any upgrades or migrations will have to be done manually (iRedMail have a good guide on [upgrading between releases](http://www.iredmail.org/docs/iredmail.releases.html) and [migrations](http://www.iredmail.org/docs/migrate.to.new.iredmail.server.html).

## Creating a New Instance ##

### Customise iRedMail Configuration ###
Customise the file `config` and place in somewhere your iRedMain container will be able to see the file (file will be mounted in the container).

Suggested folder structure:
```plain
/
+-- opt
|   +-- containers
|   |   +-- mail
|   |   |   +-- config
|   |   |   |   +-- config
|   |   |   |   +-- ...
|   |   |   +-- vmail
|   |   |   |   +-- ...
```

### Creating the Container ###
```shell
#!bash
docker run -d \
   --restart=always \
   --name mail.example.com \
   -h mail.example.com \
   -p 443:443 \
   -p 587:587 \
   -p 993:993 \
   -p 995:995 \
   -v /opt/containers/mail/config:/opt/config \
   -v /opt/containers/mail/vmail:/var/vmail \
   -e TIPS_PASSWORD=asupersecretpassword \
   kamcnally/iredmail:1.0.0
```

#### Ports ####

Apache:

* `443` - HTTPS

Postfix:

* `587` - STARTTLS encrypted SMTP

Dovecot:

* `993` - SSL/TLS encrypted IMAP
* `995` - SSL/TLS encrypted POP

#### Optional Variables ####

* `IREDMAIL_VERSION` - Override the version of iRedMail that will be installed. E.g. '0.9.5-1'. Use this at your own risk.
* `TIPS_PASSWORD` - Password used to encrypt the iRedMail tips file.

## Keeping Mail Out Of SPAM Filters (DNS Records) ##
iRedMail provide a very good set of instructions for setting up your DNS records correctly and keeping your mail out of spam filters:

- [Setup DNS records for your iRedMail server (A, PTR, MX, SPF, DKIM)](http://iredmail.org/docs/setup.dns.html)
- [Generate new DKIM key for new mail domain](http://www.iredmail.org/docs/sign.dkim.signature.for.new.domain.html#generate-new-dkim-key-for-new-mail-domain)

By default, a new DKIM key will be generated on first run of the container. You can either use this, an existing DKIM, or generate a new one.

To view the DKIM key that was generated, run:
```shell
docker exec mail.example.com amavisd-new showkeys
```

Once you have the DNS records set correctly, you can validate that it has been set up correctly by running:
```shell
docker exec mail.example.com amavisd-new testkeys
```

## Getting and Sending Mail ##

Default settings for connecting to the email server, using you client of choice, for IMAP, POP, and SMTP are below.

IMAP:

- **Server name:** mail.example.com
- **Port:** 993
- **Encryption Method:** SSL/TLS
- **Password Type:** Normal Password

POP:

- **Server name:** mail.example.com
- **Port:** 995
- **Encryption Method:** SSL/TLS
- **Password Type:** Normal Password

SMTP:

- **Server name:** mail.example.com
- **Port:** 587
- **Encryption Method:** STARTTLS
- **Password Type:** Normal Password

## Update SSL Certificates ##
SSL certificates can be copied into the container using `docker cp` and the script `/opt/certs/update_certs.sh` be run to update iRedMail certificates.

A signed certificate, domain key, and intermediate certificate must be copied into the container:
```shell
docker cp /opt/ssl/files/signed.pem mail.example.com:/opt/certs/signed.pem
docker cp /opt/ssl/files/domain.key mail.example.com:/opt/certs/domain.key
docker cp /opt/ssl/files/intermediate.pem mail.example.com:/opt/certs/intermediate.pem
```

After that, run the update script in the container to update the certificates:
```shell
docker exec mail.example.com sh -c 'cd /opt/certs && bash /opt/certs/update_certs.sh'
```

## Planned Features ##
- [x] Simple Docker container for deploying base iRedMail image
- [x] Full encryption of SMTP, IMAP, & POP
   - [x] Encrypted streams
      - [x] Encryption set to required when loading custom SSL certificates
      - [x] Encryption by default
- [x] Fully configurable at runtime; no having to bake your own image
- [x] Use custom SSL certificates
- [ ] Automated setup of new DKIM key
- [ ] Updating image/iRedMail version
   - [ ] Helper scripts to aid in backup for migration
   - [ ] Automated update when upgrading Docker image
- [ ] Automated backups