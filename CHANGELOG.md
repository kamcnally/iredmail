Unreleased
- 

1.0.1 - 20 September 2016
- Fix naming of init scripts when running SSL cert loader scripts

1.0.0 - 19 September 2016
- Initial release