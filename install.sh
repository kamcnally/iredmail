#!/bin/bash
export TERM=xterm

wget -O - --no-check-certificate \
	https://bitbucket.org/zhb/iredmail/downloads/iRedMail-"${IREDMAIL_VERSION}".tar.bz2 | \
	tar xvj

cp -rl iRedMail-"${IREDMAIL_VERSION}"/* .
rm -rf iRedMail-"${IREDMAIL_VERSION}"*

# Set hostname
echo $HOSTNAME > /etc/mailname

# Move config file into iRedMail installation directory
mv /opt/config/config /opt/iredmail/

# Install iRedMail unattended
sed -i 's/ 101/ 0/' /usr/sbin/policy-rc.d
IREDMAIL_DEBUG='NO' \
		AUTO_USE_EXISTING_CONFIG_FILE=y \
		AUTO_INSTALL_WITHOUT_CONFIRM=y \
		AUTO_CLEANUP_REMOVE_SENDMAIL=y \
		AUTO_CLEANUP_REMOVE_MOD_PYTHON=y \
		AUTO_CLEANUP_REPLACE_FIREWALL_RULES=n \
		AUTO_CLEANUP_RESTART_IPTABLES=y \
		AUTO_CLEANUP_REPLACE_MYSQL_CONFIG=y \
		AUTO_CLEANUP_RESTART_POSTFIX=n \
		bash iRedMail.sh
rm /opt/iredmail/config
apt-get purge -y -q dialog apt-utils
apt-get autoremove -y -q
apt-get clean -y -q
rm -rf /var/lib/apt/lists/*
sed -i 's/ 0/ 101/' /usr/sbin/policy-rc.d

#Create missing log files that prevent fail2ban from starting
touch /var/log/auth.log
touch /var/log/mail.log

#Enable SSL in Dovecot
sed -i 's/\([#]\?\)disable_plaintext_auth\([[:space:]=]*\)\(yes\|no\)/disable_plaintext_auth = yes/g' /etc/dovecot/conf.d/10-auth.conf
sed -i 's/\([#]\?\)ssl\([[:space:]=]*\)\(yes\|no\|required\)/ssl = required/g' /etc/dovecot/conf.d/10-ssl.conf
sed -i 's/\([#]\?\)ssl_cert\([[:space:]=]*\)<\(.*\)/ssl_cert = <\/etc\/ssl\/certs\/iRedMail.crt/g' /etc/dovecot/conf.d/10-ssl.conf
sed -i 's/\([#]\?\)ssl_key\([[:space:]=]*\)<\(.*\)/ssl_key = <\/etc\/ssl\/private\/iRedMail.key/g' /etc/dovecot/conf.d/10-ssl.conf

#Configure Roundcube to have some sensible defaults
sed -i "s/\([\/].\?\)\$config\['username_domain']/\$config\['username_domain']/g"  /opt/www/roundcubemail/config/config.inc.php

if [ -z "$TIPS_PASSWORD" ]; then
    echo "TIPS_PASSWORD is empty, tips file will be left in plain text"
	mv /opt/iredmail/iRedMail.tips /opt/config/
else
    echo "Encrypting tips file"
	echo $TIPS_PASSWORD | openssl enc -e -aes-256-cbc -pass stdin -in /opt/iredmail/iRedMail.tips -out /opt/iredmail/iRedMail.tips.enc
	mv  /opt/iredmail/iRedMail.tips.enc /opt/config/
fi

rm -- "$0"