#!/bin/bash
export TERM=xterm

ps auxw | grep postfix | grep -v grep | grep -v $(basename -- "$0") > /dev/null

if [ $? != 0 ]
then
	echo Postfix is not running. Starting.
	postfix start > /dev/null
else
	echo Postfix is running. Reloading.
	postfix reload > /dev/null
fi