#!/bin/bash
export TERM=xterm

ps auxw | grep apache2 | grep -v grep | grep -v $(basename -- "$0") > /dev/null

if [ $? != 0 ]
then
	echo Apache is not running. Starting.
	/etc/init.d/apache2 start > /dev/null
else
	echo Apache is running. Reloading.
	/etc/init.d/apache2 reload > /dev/null
fi