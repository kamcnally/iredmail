#!/bin/bash
export TERM=xterm

ps auxw | grep mysql | grep -v grep | grep -v $(basename -- "$0") > /dev/null

if [ $? != 0 ]
then
	echo MySQL is not running. Starting.
	/etc/init.d/mysql start
else
	echo MySQL is running. Reloading.
	/etc/init.d/mysql reload
fi
