#!/bin/bash
export TERM=xterm

ps auxw | grep amavis | grep -v grep | grep -v $(basename -- "$0") > /dev/null

if [ $? != 0 ]
then
	echo Amavis is not running. Starting.
	service amavis start > /dev/null
else
	echo Amavis is running. Reloading.
	service amavis force-reload > /dev/null
fi