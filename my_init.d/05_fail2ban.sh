#!/bin/bash
export TERM=xterm

ps auxw | grep fail2ban | grep -v grep | grep -v $(basename -- "$0") > /dev/null

if [ $? != 0 ]
then
	echo fail2ban is not running. Starting.
	/etc/init.d/fail2ban start
else
	echo fail2ban is running. Restarting.
	/etc/init.d/fail2ban restart
fi