#!/bin/bash
export TERM=xterm

ps auxw | grep iredapd | grep -v grep | grep -v $(basename -- "$0") > /dev/null

if [ $? != 0 ]
then
	echo iRedAPD is not running. Starting.
	service iredapd start > /dev/null
else
	echo iRedAPD is running. Restarting.
	service iredapd restart > /dev/null
fi