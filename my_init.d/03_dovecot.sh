#!/bin/bash
export TERM=xterm

ps auxw | grep dovecot | grep -v grep | grep -v $(basename -- "$0") > /dev/null

if [ $? != 0 ]
then
	echo Dovecot is not running. Starting.
	dovecot > /dev/null
else
	echo Dovecot is running. Reloading.
	dovecot reload > /dev/null
fi