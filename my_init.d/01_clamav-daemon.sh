#!/bin/bash
export TERM=xterm

ps auxw | grep clamav | grep -v grep | grep -v $(basename -- "$0") > /dev/null

if [ $? != 0 ]
then
	echo Clamav daemon is not running. Starting.
	service clamav-daemon start > /dev/null
else
	echo Clamav daemon is running. Reloading.
	service clamav-daemon force-reload > /dev/null
fi