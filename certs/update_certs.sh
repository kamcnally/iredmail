#!/bin/bash
export TERM=xterm

if [ ! -f /opt/certs/signed.pem ]; then
    echo "Certificate not found."
	exit 1
fi

if [ ! -f /opt/certs/domain.key ]; then
    echo "Certificate key not found."
	exit 1
fi

if [ ! -f /opt/certs/intermediate.pem ]; then
    echo "Certificate intermediate key not found."
	exit 1
fi

chmod 0444 signed.pem
chmod 0400 domain.key
chmod 0444 intermediate.pem

#Overwrite iRedMail certificates
cp /opt/certs/signed.pem /etc/ssl/certs/iRedMail.crt
cp /opt/certs/domain.key /etc/ssl/private/iRedMail.key
cp /opt/certs/intermediate.pem /etc/ssl/certs/iRedMail.ca

#Enable CA intermediate certificate
sed -i 's/\([#]\?\)SSLCertificateChainFile\([[:space:]]*\)\(.*\)/SSLCertificateChainFile \/etc\/ssl\/certs\/iRedMail.ca/g' /etc/apache2/sites-available/default-ssl.conf

#Retart services
/etc/my_init.d/03_postfix.sh
/etc/my_init.d/03_dovecot.sh
/etc/my_init.d/04_apache.sh

#Cleanup for next run
rm -f /opt/certs/signed.pem
rm -f /opt/certs/domain.key
rm -f /opt/certs/intermediate.pem